console.log("HALLO WORLD");
var RD_crs = new L.Proj.CRS('EPSG:28992',
  '+proj=sterea +lat_0=52.15616055555555 +lon_0=5.38763888888889 +k=0.9999079 +x_0=155000 +y_0=463000 +ellps=bessel +units=m +no_defs',
  {
    origin: [-285401.92, 22598.08],
    resolutions: [
      3440.64, 1720.32, 860.16, 430.08, 215.04, 107.52, 53.76, 26.88, 13.44, 6.72, 3.36, 1.68, 0.84, 0.42, 0.21
    ],
    bounds: L.bounds([[-285401.92, 22598.08], [595401.92, 903401.92]])
  });

var mymap = new L.Map('map', {
  crs: RD_crs,
}).setView([52, 4], 2);


_getParent = function(element, className) {
  
  var parent = element.parentNode;
  
  while (parent != null) {
    
    if (parent.className && L.DomUtil.hasClass(parent, className))
      return parent;
    
    parent = parent.parentNode;
    
  }
  
  return false;
  
}

var shown_layers = [];
var selector = $('#shapelist');
var selected_uuid = "C1DD0762-7D3F-49E5-934A-69A7B6B8082D";
selector.on("change", function(){
  selected_uuid = selector.val().split(" - ")[1];
  console.log(selected_uuid);
  load_area();
})

// Alle gebieden en uuid's verkrijgen zonder shapes
$.get('https://geoserverpzh1.westeurope.cloudapp.azure.com/geoserver/ows?service=wfs&version=2.0.0&request=GetFeature&outputFormat=application/json&typeName=Deelgebieden&propertyName=UUID,Gebied',
  function(data, status) {
    selector.html("")
    for (var f in data.features) {
      selector.append("<option>" + data.features[f].properties['Gebied'] + " - " + data.features[f].properties['UUID'] + "</option>");
    };
  });

function load_area() {
  //Leaflet-wfst gebruiken om de datalaag in te laden
  var deelgebieden = new L.WFS({
    url: 'https://geoserverpzh1.westeurope.cloudapp.azure.com/geoserver/ows',
    crs: RD_crs,
    typeNS: 'OMGEVINGSBELEID',
    typeName: 'Deelgebieden',
    fillOpacity: 0.5,
    style: {
     color: 'blue',
     weight: 1,
     fill: true,
    },
    filter: new L.Filter.EQ('UUID', selected_uuid),
  });
  shown_layers.forEach(function (layer) {
    mymap.removeLayer(layer);
  });
  $('#message').html("Loading...")
  deelgebieden.on('load', function() {
    $('#message').html("");
  })
  shown_layers = [];
  shown_layers.push(deelgebieden);
  deelgebieden.addTo(mymap);
};
//Tiled background map
var BRT_achtergrondkaart = L.tileLayer('https://geodata.nationaalgeoregister.nl/tiles/service/tms/1.0.0/brtachtergrondkaart/EPSG:28992/{z}/{x}/{y}.png', {
    maxZoom: RD_crs.options.resolutions.length,
    minZoom: 2,
    continuousWorld: true,
    tms: true,
    attribution: 'Map data: <a href="http://www.kadaster.nl">Kadaster</a>'
});

BRT_achtergrondkaart.addTo(mymap);
